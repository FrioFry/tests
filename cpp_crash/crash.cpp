// g++ (Ubuntu 5.4.0-6ubuntu1~16.04.9) 5.4.0 20160609
// g++ -Wall -std=c++1y -O3 -flto crash.cpp

#include <iostream>
#include <map>

#include <cstdint>
#include <cstddef>
#include <cstring>

struct BinaryDataReader
{
    virtual ~BinaryDataReader() = default;
};

struct BinaryDataWriter
{
    virtual ~BinaryDataWriter() = default;
    virtual void Write(size_t size, void const* buffer) = 0;
};

// Перестает падать при изменении порядка базовых классов
class MemoryBinaryData
    : public BinaryDataReader
    , public BinaryDataWriter
{
public:
    MemoryBinaryData()
    {
    }

    ~MemoryBinaryData() override {
    }

public: // BinaryDataWriter
    void Write(size_t size, void const* buffer) override {
        memcpy(data_, buffer, size);
    }

private:
    uint8_t data_[1024];
};

void Write(BinaryDataWriter& writer, int const& val)
{
    // Если вывести, падать не будет.
    // std::cout << "Write<T>: " << &val << std::endl;
    writer.Write(sizeof(int), &val);
}

int main()
{
    MemoryBinaryData memory;
    std::map<uint32_t, float> data;
    data[5] = 500.;
    int a = 0xdeadf00d;

    for (auto const& pair : data)
    {
        Write(memory, a);
        // Так не падает
        // memory.Write(sizeof(a), &a);
    }
}
