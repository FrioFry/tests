#! /bin/sh

export PATH=/usr/local/cuda-9.1/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-9.1/lib64\
	 ${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}

nvcc 1.cu

./a.out
