#! /usr/bin/python

import time
import os
import commands

#translate = "http://translate.google.com/translate?js=n&sl=auto&tl=en&u=%s"

track_url = "https://www.laposte.fr/particulier/outils/suivre-vos-envois?code=6C13715385652"

#url = translate % track_url

while True:
    try:
        lines = commands.getstatusoutput('w3m -dump %s | grep -o ../03/2018 ' % track_url)[1].split("\n")
        print lines
        lines = [l for l in lines if l.strip()]
        lines = [int(l.split("/")[0]) for l in lines if int(l.split("/")[0])>= 22]

        if len(lines) > 3:
            os.system('notify-send -i face-wink "new message!"')
    except Exception:
        pass
    finally:
        time.sleep(60)
