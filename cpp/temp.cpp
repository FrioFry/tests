#include <iostream>
const int NewContactId = -1;
int main() {
    struct A {
        int b = 10;
        int c = {11};
        int d = NewContactId;
    };
    A a;
    std::cout << " 1 " << std::endl;
    std::cout << a.b << std::endl;
    std::cout << a.c << std::endl;
    std::cout << a.d << std::endl;
    return 1;
}
