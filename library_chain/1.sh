gcc -c -Wall -fpic lib1.c
gcc -shared -o liblib1.so lib1.o

gcc -c -Wall -fpic lib2.c
gcc -shared -o liblib2.so lib2.o -llib1 -L.

gcc -L. -Wall -o test main.c -llib1 -llib2
