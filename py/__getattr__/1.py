import sys

class memoized(object):
    """Decorator that caches a function's return value each time it is called.
    If called later with the same arguments, the cached value is returned, and
    not re-evaluated.
    """
    def __init__(self, func):
        self.func = func
        self.cache = {}

    def __call__(self, *args):
        try:
            return self.cache[args]
        except KeyError:
            value = self.func(*args)
            self.cache[args] = value
            return value
        except TypeError:
            # uncachable -- for instance, passing a list as an argument.
            # Better to not cache than to blow up entirely.
            return self.func(*args)

    def __repr__(self):
        """Return the function's docstring."""
        return self.func.__doc__

    def __get__(self, obj, objtype):
        """Support instance methods."""
        return lambda *args: self.__call__(obj, *args)

class SystemHelper(object):
    def __init__(self):
        pass
    
    _data_getter_map = {
        "cpu": lambda : "cpu",
        "memory": lambda : "memory"
    }

    @memoized
    def _get_data(self, name):
        return self._data_getter_map[name]

    def __getattr__(self, name):
        try:
            return self._get_data(name)
        except KeyError:
            _logger.error("unsupported value %s" % name)
        return ""

s  = SystemHelper ()
print s.cpu()
#print s.memory