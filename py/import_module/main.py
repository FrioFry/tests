#! /usr/bin/python
from helpers.helper_loader import HelperLoader

def get_loader():
    return {"mgmt": HelperLoader()}


def main():
    l = get_loader()
    mgmt = l["mgmt"]
    disk_info = mgmt.disk_info
    print disk_info
    info = disk_info.info()
    print info


if __name__ == "__main__":
    main()
