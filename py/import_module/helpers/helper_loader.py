import logging
import importlib

LOG_FILENAME = 'example.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)
logger = logging.getLogger(__package__)


class HelperLoader(object):
    def __getattr__(self, name):
        try:
            logger.error("accessing %s_helper near %s" % (name, str(__package__)))
            mod = importlib.import_module(".%s_helper" % name, __package__)
            m = getattr(mod, name + "_helper")()
        except Exception as ex:
            logger.exception("failed to load %s_helper from %s" %
                             (name, str(__package__)))
            m = {}
        setattr(self, name, m)
        return m
