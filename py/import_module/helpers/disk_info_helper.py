import logging

LOG_FILENAME = 'example.log'
logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG)
logger = logging.getLogger(__package__)


class disk_info_helper(object):
    def __init__(self):
        logger.error("init diskinfohelper")

    def info(self):
        result = {"sda": {"disks": 11}}
        return result
