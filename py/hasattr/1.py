import importlib
import sys

class UserHelper(object):
    __auto_load__ = False
    def __init__(self, name):
        self.name = name


helper_class = UserHelper
if hasattr(helper_class, '__auto_load__') and not helper_class.__auto_load__:
    m = {}
else:
    m = helper_class("abc")

print m
