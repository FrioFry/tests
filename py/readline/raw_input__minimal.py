#!/usr/bin/python
# Written by Sebastian Pipping <sebastian@pipping.org>
# Licensed under CC0 1.0 (https://creativecommons.org/publicdomain/zero/1.0/)
# 2015-05-22 16:51 UTC+2

from __future__ import print_function

import readline
import sys


if sys.version_info[0] > 2:
    raw_input = input


if __name__ == '__main__':
    readline.add_history('some command to search for')

    print('Press Ctrl+D to leave.')
    print('For the bug to show: press <Ctrl+R> (for search), type "a", press <Ctrl+C>, press <Return>.')

    while True:
        try:
            line = raw_input('> ')
        except EOFError:
            print()
            break
        except KeyboardInterrupt:
            print()
            continue

        print('You wrote "%s".' % line)
