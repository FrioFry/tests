#! /usr/bin/python
import atexit
import os
#import readline
import gnureadline as readline

def print_arg(arg):
    print arg

def main():
    histfile = "/home/ricko/.pyhist"
    print histfile
    try:
        readline.read_history_file(histfile)
        readline.set_history_length(5)
    except IOError:
        pass

    #atexit.register(readline.write_history_file, histfile)
    while True:
        try:
            rv = raw_input("console> ")
        except Exception:
            readline.write_history_file(histfile)
            break

main()
