#!/usr/bin/env python
import os
from spooler import spooler

if __name__ == "__main__":
    os.nice(40)
    spooler.process_jobs()
