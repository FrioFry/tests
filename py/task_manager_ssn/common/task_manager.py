import os
import importlib
import json
import glob
import Queue
import time
import threading
import pyinotify


# search in tasks.py
_TASK_QUEUES = {
    "q1": {
        "max_tasks_in_parallel": 1
    },
    "q2": {
        "max_tasks_in_parallel": 2
    }
}

JOB_EXT = "task"

def make_queue_item(path):
    return path

class EventHandler(pyinotify.ProcessEvent):
    def __init__(self, queue, *args, **kwargs):
        super(EventHandler, self).__init__(*args, **kwargs)
        self.queue = queue

    def process_IN_CLOSE_WRITE(self, event):
        p = event.pathname
        if p.endswith(".{}".format(JOB_EXT)):
            self.queue.put(make_queue_item(event.pathname))
            print "Adding to queue: %s"% event.pathname

def resolve(route):
    path, name = route.rsplit(".", 1)
    m = importlib.import_module(path)
    return getattr(m, name)

def get_job_name(path):
    return os.path.splitext(os.path.basename(path))[0]

class Spooler(object):
    def __init__(self, jobs_path, num_workers, spool_jobs):
        self.num_workers = num_workers
        self.jobs_path = jobs_path
        self.queue = Queue.PriorityQueue()
        self.spool_jobs = spool_jobs

    def _parse_job(self, path):
        with open(path, "r") as f:
            blob = f.read()
            data = json.loads(blob)
            try:
                print "Parsed job for route {}".format(data["route"])
                return resolve(data["route"]), data["data"]
            except KeyError, e:
                raise ValueError("Invalid job data: {0}".format(str(e)))

    def _do_one_task(self, task, thread_id):
        path = task
        name = get_job_name(path)
        print "{}: Started processing {}".format(
            thread_id, name)

        fn, data = self._parse_job(path)
        try:
            #with db.transaction.atomic():
            fn(data)
        except Exception, e:
            print "blablah blah error ! failed FUIII=UUi"
        else:
            os.remove(path)
        finally:
            pass
        print "{}: Finished processing {}".format(thread_id, name)

    def _worker(self):
        pid = os.getpid()
        thread = threading.current_thread()
        thread_id = "{}.{}".format(pid, thread.ident)

        print "{}: Starting spooler worker".format(thread_id)
        while True:
            try:
                print "before task get %s" % self.queue.qsize()
                task = self.queue.get(block=True)
                self._do_one_task(task, thread_id)
            except Exception, e:
                message = "{}: Error while processing job: {}".format(
                    thread_id, e)
                print message
            finally:
                self.queue.task_done()

    def process_jobs(self):
        for _ in range(self.num_workers):
            t = threading.Thread(target=self._worker)
            t.daemon = True
            t.start()

        wm = pyinotify.WatchManager()
        wm.add_watch(self.jobs_path, pyinotify.IN_CLOSE_WRITE)

        handler = EventHandler(self.queue)
        notifier = pyinotify.Notifier(wm, handler)

        for path in sorted(glob.glob("{}/*.{}".format(
                self.jobs_path, JOB_EXT))):
            self.queue.put(make_queue_item(path))
        print "a task get %s" % self.queue.qsize()

        # we do not process inotify queue overflow
        notifier.loop()

    def _format_job(self, route, data=None):
        try:
            resolve(route)
        except:
            raise ValueError("Unable to resolve route: {}".format(route))


        result = {"route": route, "data": data}
        return json.dumps(result)

    def add_job(self, route, data=None, name=""):
        thread = threading.current_thread()
        pid = os.getpid()
        thread_id = "{}.{}".format(pid, thread.ident)
        name = "{:f}.{}.{}".format(time.time(), thread_id, name)
        print "{}: Adding task {}".format(thread_id, name)
        path = "{}/{}.{}".format(self.jobs_path, name, JOB_EXT)
        job = self._format_job(route, data)
        with open(path, "w") as f:
            f.write(job)
        return path
