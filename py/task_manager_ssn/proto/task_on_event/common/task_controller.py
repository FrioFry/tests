# -*- coding: utf-8 -*-
from collections import deque
import threading
import importlib


def resolve(route):
    path, name = route.rsplit(".", 1)
    m = importlib.import_module(path)
    return getattr(m, name)


class TaskController:
    def __init__(self, task_queues, task_source):
        self.queues = task_queues
        self.source = task_source
        self.processed_tasks = set()
        self.lock = threading.Lock()

    def on_update(self):
        tasks = self.source.get_tasks()
        with self.lock:
            # Отфильтровать уже обработанные задачи
            new_tasks = set(tasks.keys()) - self.processed_tasks
            print "New tasks: %s" % new_tasks
            # Добавить id в processed_tasks
            self.processed_tasks = self.processed_tasks.union(new_tasks)
            for task_name in new_tasks:
                try:
                    task_data = tasks[task_name]

                    # распарсить параметры задачи
                    route = task_data["route"]
                    id = task_data["id"]
                    args = task_data["data"]
                    queue = task_data["queue"]
                    priority = task_data.get("priority")

                    # Для каждой задачи сделать обертку
                    def run_and_remove(route, args, id, task_source):
                        def _wrapped():
                            fn = resolve(route)
                            fn(args)
                            task_source.remove_task(id)
                        return _wrapped
                    wrapped_fn = run_and_remove(route, args, id, self.source)

                    # Найти очередь
                    # если очередь есть, добавить в очередь
                    if queue in self.queues:
                        task_queue = self.queues[queue]
                        task_queue.add_task(wrapped_fn, priority)
                    else:
                    # если очереди нет, выполнить задачу в потоке
                        t = threading.Thread(target=wrapped_fn)
                        t.daemon = True
                        t.start()
                except Exception as ex:
                    print "Failed to add task %s: %s" % (task_name, ex)
