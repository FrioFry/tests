import os
import json
import time


_TASK_EXT = "task"
_TASKS_DIR  = "/home/ricko/tests/py/task_manager_ssn/proto/task_on_event/tasks"


class TaskSourceBase(object):
    def get_tasks(self):
        # returns {task_name: task_content}
        return {}

    def add_task(self, name, route, queue="", data={}):
        pass

    def remove_task(self, task_name):
        pass


def _task_to_file(task_name):
    return os.path.join(_TASKS_DIR, task_name + "." + _TASK_EXT)


def _read_task(filename):
    with open(os.path.join(_TASKS_DIR, filename), "r") as f:
        blob = f.read()
        data = json.loads(blob)
        return data

def _read_fs_tasks():
    t = [x for x in os.listdir("tasks") if x.endswith("." + _TASK_EXT)]
    ret = {x: _read_task(x) for x in t}
    return ret


def _write_fs_task(task_name, data):
    path = _task_to_file(task_name)
    with open(path, "w") as f:
        f.write(json.dumps(data))


def _remove_fs_task(task_name):
    os.unlink(_task_to_file(task_name))


class FSTaskSource(TaskSourceBase):
    def get_tasks(self):
        return _read_fs_tasks()

    def add_task(self, name, route, queue="", data={}):
        task_name = "{:f}.{}.{}".format(time.time(), name, queue)
        print "Task name: %s" % task_name
        data = {
            "route": route,
            "queue": queue,
            "name": name,
            "data": data,
            "id": task_name
        }
        _write_fs_task(task_name, data)

    def remove_task(self, task_name):
        _remove_fs_task(task_name)
