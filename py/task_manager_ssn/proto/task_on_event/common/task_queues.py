import Queue
import threading

RESCHEDULED, VERY_LOW, LOW, NORMAL, HIGH = (50, 40, 30, 20, 10)

_TASK_QUEUES = {
    "queue1": {
    	"max_tasks_in_parallel": 5,
    	"discard_tasks_if_queue_is_full": True
    },
    "queue2": {
    	"max_tasks_in_parallel": 20,
    	"discard_tasks_if_queue_is_full": False
    }
}

def parse_queue_decl(queue_decl=_TASK_QUEUES):
    res = {}
    for k, v in queue_decl.iteritems():
        max_tasks = v["max_tasks_in_parallel"]
        discard_if_full = v["discard_tasks_if_queue_is_full"]
        tq = TaskQueue(max_tasks, discard_if_full)
        res[k] = tq
    return res


class TaskConsumer(threading.Thread):
    def __init__(self, queue):
        threading.Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            task = self.queue.get()
            self.queue.task_done()


class TaskQueue:
    def __init__(self, max_tasks, discard_if_full):
        self.queue = Queue.PriorityQueue()
        for _ in range(max_tasks):
            t = TaskConsumer(self.queue)
            t.daemon = True
            t.start()

    def add_task(self, fn, priority):
        self.queue.put((priority, fn or NORMAL))

    def get_task(self):
        try:
            return self.queue.get_nowait()
        except Queue.Empty:
            return None
