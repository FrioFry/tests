#! /usr/bin/python
import signal
import os
import time
from common.task_source import FSTaskSource
from common.task_queues import parse_queue_decl
from common.task_controller import TaskController

def initialize_queues():
    return parse_queue_decl()

queues = {}
fs_task_source = None
task_controller = None

def receive_signal(signum, stack):
    print 'Received:', signum
    task_controller.on_update()

def main():
    global queues
    global fs_task_source
    global task_controller

    queues = initialize_queues()
    fs_task_source = FSTaskSource()
    task_controller = TaskController(queues, fs_task_source)

    print "register SIGUSR1"
    print 'My PID is:', os.getpid()
    signal.signal(signal.SIGUSR1, receive_signal)

    while True:
        signal.pause()
        #time.sleep(3)

if __name__ == "__main__":
    main()
