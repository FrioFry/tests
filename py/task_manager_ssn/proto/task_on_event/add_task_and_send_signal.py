#! /usr/bin/python
import signal
import os
import time
import random
from common.task_source import FSTaskSource

from subprocess import check_output

task_source = None

def get_pid(name):
    return check_output(["pidof", "-x", name])

def get_catcher_pid():
    ret = get_pid("signal_catcher.py")
    ret = int(ret.split()[0])
    return ret


def send_signal(pid):
    print "sending SIGUSR to %s" % pid
    os.kill(pid, signal.SIGUSR1)
    print "done?"

def _add_random_task():
    x = int(random.random() * 3) + 1
    #qq = ["queue1", "queue2", None, ""]
    qq = ["queue1"]
    task_source.add_task(
        name="task1",
        route="module1.func1.job_handler",
        queue=random.choice(qq),
        data={"time": x}
        )
    print "Added %f" %x


def add_tasks():
    global task_source
    #x = int(random.random()*3)
    #for _ in range(x):
    _add_random_task()


def main():
    global task_source
    task_source = FSTaskSource()
    while True:
        try:
            add_tasks()

            print "searching for 'signal_catcher.py'"
            pid = get_catcher_pid()
            print 'found pid %s' % pid
            send_signal(pid)
        except Exception as ex:
            print "failed to add task %s", ex
        finally:
            time.sleep(3)


if __name__ == "__main__":
    main()
