#! /usr/bin/python
import signal
import os

def receive_signal(signum, stack):
    print 'Received:', signum

def main():
    print "register SIGUSR1"
    print 'My PID is:', os.getpid()
    signal.signal(signal.SIGUSR1, receive_signal)
    while True:
        signal.pause()

if __name__ == "__main__":
    main()
