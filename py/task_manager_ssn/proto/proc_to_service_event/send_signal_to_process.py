#! /usr/bin/python
import signal
import os
import time

from subprocess import check_output

def get_pid(name):
    return check_output(["pidof", "-x", name])

def get_catcher_pid():
    ret = get_pid("signal_catcher.py")
    ret = int(ret.split()[0])
    return ret


def send_signal(pid):
    print "sending SIGUSR to %s" % pid
    os.kill(pid, signal.SIGUSR1)
    print "done?"

def main():
    while True:
        print "searching for 'signal_catcher.py'"
        pid = get_catcher_pid()
        print 'found pid %s' % pid

        send_signal(pid)
        time.sleep(3)

if __name__ == "__main__":
    main()
