from spooler import spooler
import random

if __name__ == "__main__":
    data={random.random(): random.random()}
    spooler.add_job("module1.func1.job_handler", name="job_handler", data=data)
