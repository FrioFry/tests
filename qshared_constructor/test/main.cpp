#include <QCoreApplication>
#include <QSharedPointer>
#include <QDebug>
#include <QHash>

struct TextGroup {
    QByteArray guid;
    bool operator==(const TextGroup &rhs) const
    {
        qDebug() << Q_FUNC_INFO;
        return guid == rhs.guid;
    }
};

struct TextThread {
    int id = -1;

    QSharedPointer<TextGroup> group;

    bool operator==(const TextThread &rhs) const
    {
        qDebug() << Q_FUNC_INFO;
        bool sameGroup = group == rhs.group;
        if (!sameGroup && group && rhs.group) {
            sameGroup = *group == *rhs.group;
        }

        return id == rhs.id && sameGroup;
    }

    TextThread& operator=(const TextThread &other) {
        qDebug() << Q_FUNC_INFO;
        if (this != &other) {
            id = other.id;
            if (other.group) {
                group = QSharedPointer<TextGroup>::create(*other.group);
            }
        }
        return *this;
    }

    TextThread() = default;

    TextThread(const TextThread &other) {
        qDebug() << Q_FUNC_INFO;
        id = other.id;
        if (other.group) {
            group = QSharedPointer<TextGroup>::create(*other.group);
        }
    }
};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QHash<int, TextThread> _threads;

    qDebug() << "1: ";
    TextThread t1;
    t1.id = 1;
    qDebug() << "assign";
    _threads[1] = t1;

    qDebug() << "2: ";
    TextThread t2;
    t2.id = 2;
    TextGroup g2;
    g2.guid = QString("222").toUtf8();
    t2.group = QSharedPointer<TextGroup>::create(g2);

    qDebug() << "3: ";
    QSharedPointer<TextThread> t3 = QSharedPointer<TextThread>::create(t1);
    qDebug() << "4: ";
    QSharedPointer<TextThread> t4 = QSharedPointer<TextThread>::create(t2);
    qDebug() << "4 group: ";
    qDebug() << QString::fromUtf8(t4->group->guid);
    return a.exec();
}
