%module example
%include "stl.i"
%include "exception.i"

%{
#define SWIG_FILE_WITH_INIT
#include "example.h"
%}


%exception {
    try {
        $action
    }
    catch (const std::exception &e)
    {
        SWIG_exception(SWIG_RuntimeError, e.what());
    }
}

%include "example.h"
