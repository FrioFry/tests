
function onSizeChanged(sz) {
  console.log(sz);
  scaleElement(0, sz);
}

function scaleElement(id, factor) {
    let big = document.getElementById("big-logo");
    let normal = document.getElementById("normal-logo");
    big.style.transform = "scale(" + factor + "," + factor + ")";
    normal.style.transform = "scale(" + factor + "," + factor + ")";
}
