#! /usr/bin/python

import subprocess
import os
mode = subprocess.check_output(["gsettings", "get", "org.gnome.system.proxy" ,"mode"]).strip()

if mode == "'none'":
    subprocess.check_output(["gsettings", "set", "org.gnome.system.proxy.socks" ,"host", "'droplet'"])
    subprocess.check_output(["gsettings", "set", "org.gnome.system.proxy.socks" ,"port", "80"])
    subprocess.check_output(["gsettings", "set", "org.gnome.system.proxy" ,"mode", "'manual'"])
    os.system('notify-send -i face-wink "Proxy On!"')

else:
    subprocess.check_output(["gsettings", "set", "org.gnome.system.proxy" ,"mode", "'none'"])
    os.system('notify-send -i face-wink "Proxy Off!"')



print subprocess.check_output(["gsettings", "get", "org.gnome.system.proxy" ,"mode"]).strip()
