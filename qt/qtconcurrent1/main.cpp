#include <QtCore>
#include <QtConcurrent/QtConcurrent>
#include <QDebug>

#include <type_traits>

QString myToUpper(const QString &str) {
    return str.toUpper();
}



int main(int argc, char **argv) {
    QCoreApplication app(argc, argv);

    QStringList lst;
    for (int i = 0; i < 1000; i++) {
        lst << "abc ";
    } 
    QTread t()
    QFuture<QString> future = QtConcurrent::mapped(lst.begin(), lst.end(), myToUpper);
    future.waitForFinished();
    qInfo() << future.results();
    return 0;
}
