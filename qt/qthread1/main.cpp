#include <QApplication>
#include <QThread>
#include <QProgressBar>
#include <type_traits>

class MyThread: public QThread {
    Q_OBJECT

public:
    void run() {
        for (int i = 0; i <= 100; ++i) {
            usleep(100000);
            emit progress(i);
        }
    }
signals:
    void progress(int);
};

int main(int argc, char**argv) 
{
    QApplication app(argc, argv);
    QProgressBar prb;
    MyThread thread;
    static_assert(std::is_base_of<QObject, MyThread>::value);
    QObject::connect(&thread, &MyThread::progress, &prb, &QProgressBar::setValue);

    prb.show();
    thread.start();
    return app.exec();
}

#include "main.moc"