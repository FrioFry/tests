#include "controller.h"
#include <QDir>
#include <QDebug>
#include <QStandardPaths>
#include <QDesktopServices>
#include <QProcess>
#include <libsnore/snore.h>
#include <libsnore/snore_p.h>
#include <libsnore/utils.h>
#include <libsnore/application.h>
#include <libsnore/notification/notification.h>
#include "libsnore/snore_static_plugins.h"
Controller::Controller(QObject *parent)
    : QObject(parent)
{
    Snore::SnoreCore::instance().loadPlugins(Snore::SnorePlugin::All);
}

void Controller::testNotification()
{
    qDebug() <<"clicked";
    Snore::SnoreCore::instance().displayExampleNotification();
}
