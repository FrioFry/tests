QT += quick qml core gui network dbus multimedia widgets
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
macx {
    LIBS += -framework AppKit -framework Security -framework Foundation
}

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        controller.cpp \
        main.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH *= $${PWD}/snorenotify/include

DEFINES *= SNORE_STATIC
QT += dbus quick




LIBS += -L$${PWD}/snorenotify/lib/ -lsnore-qt5 -llibsnore_backend_osxnotificationcenter -llibsnore_backend_snore -lsnoresettings-qt5 -llibsnore_static_plugins -llibsnore_backend_trayicon \
-llibsnore_settings_backend_snore -llibsnore_settings_secondarybackend_toasty -llibsnore_secondarybackend_toasty

HEADERS += \
    controller.h
