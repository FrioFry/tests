#ifndef SNORE_STATIC_PLUGINS_H
#define SNORE_STATIC_PLUGINS_H

#include <QCoreApplication>
#include <QtPlugin>

#define SNORE_STATIC 1
#define SNORE_STATIC_QT 0
#define Qt5Quick_FOUND 1


#ifndef SNORE_CONFIG_ONLY
#if SNORE_STATIC_QT

#if Qt5Quick_FOUND
    Q_IMPORT_PLUGIN(QtQuick2Plugin)
    Q_IMPORT_PLUGIN(QtQuick2WindowPlugin)
#endif

#endif

#if SNORE_STATIC
namespace SnorePlugin {}


using namespace SnorePlugin;

Q_IMPORT_PLUGIN(Snore)
Q_IMPORT_PLUGIN(OSXNotificationCenter)

//namespace {
    static void loadSnoreResources()
    {
        // prevent multiple symbols
         static const auto load = []() {
             Q_INIT_RESOURCE(snore);
             Q_INIT_RESOURCE(snore_notification);
         };
         load();
    }
//}
Q_COREAPP_STARTUP_FUNCTION(loadSnoreResources)
#endif

#endif
#endif
