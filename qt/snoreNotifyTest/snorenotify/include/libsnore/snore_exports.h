
#ifndef SNORE_EXPORT_H
#define SNORE_EXPORT_H

#ifdef LIBSNORE_STATIC_DEFINE
#  define SNORE_EXPORT
#  define LIBSNORE_NO_EXPORT
#else
#  ifndef SNORE_EXPORT
#    ifdef libsnore_EXPORTS
        /* We are building this library */
#      define SNORE_EXPORT 
#    else
        /* We are using this library */
#      define SNORE_EXPORT 
#    endif
#  endif

#  ifndef LIBSNORE_NO_EXPORT
#    define LIBSNORE_NO_EXPORT 
#  endif
#endif

#ifndef LIBSNORE_DEPRECATED
#  define LIBSNORE_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef LIBSNORE_DEPRECATED_EXPORT
#  define LIBSNORE_DEPRECATED_EXPORT SNORE_EXPORT LIBSNORE_DEPRECATED
#endif

#ifndef LIBSNORE_DEPRECATED_NO_EXPORT
#  define LIBSNORE_DEPRECATED_NO_EXPORT LIBSNORE_NO_EXPORT LIBSNORE_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef LIBSNORE_NO_DEPRECATED
#    define LIBSNORE_NO_DEPRECATED
#  endif
#endif

#endif /* SNORE_EXPORT_H */
