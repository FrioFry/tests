#ifndef Controller_HPP
#define Controller_HPP

#include <QObject>
#include <QVariant>
#include <QUrl>
#include <QProcess>

class Controller : public QObject
{
    Q_OBJECT
public:
    explicit Controller(QObject *parent = nullptr);

    Q_INVOKABLE void testNotification();

public slots:
signals:

private:
};

Q_DECLARE_METATYPE(Controller *)

#endif // Controller_HPP
