import os, sys, json

import PySide2.QtQml
from PySide2.QtGui import QGuiApplication
from PySide2.QtQml import QQmlApplicationEngine

from qml_rc import *
from aws.aws_state import AwsState

def main():
    #Set up the application window
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine()
    ctx = engine.rootContext()
    ctx.setContextProperty("qmlapp", engine)

    # Properties
    aws = AwsState()
    ctx.setContextProperty("aws", aws)

    qml_file = os.path.join(os.path.dirname(__file__), "main.qml")
    engine.load(qml_file)

    #Show the window
    win = engine.rootObjects()[0]
    win.show()

    #execute and cleanup
    app.exec_()


if __name__ == "__main__":
    main()
