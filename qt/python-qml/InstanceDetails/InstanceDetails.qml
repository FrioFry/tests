import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

import ".."

Item {
    id: root
    property string instanceId
    property bool sshAvailable: aws.instanceSshAvailable[instanceId] || false
    property var description: aws.instanceDescription
    property var consoleOutput: []
    property string currentView: "details"
    signal close

    function refresh() {
        if (currentView == "details") {
            aws.describe_instance(instanceId)
            if (!sshAvailable) {
                aws.check_instance_ssh(instanceId)
            }
        }
    }

    Timer {
        id: refreshTimer
        interval: 10000
        repeat: true
        running: visible
        onTriggered: {
            refresh()
        }
    }

    onVisibleChanged:  {
        if (visible) {
            refresh()
        }
    }

    function showDetails() {
        currentView = "loader"
    }

    function showSshConsole() {
        currentView = "sshConsole"
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 32

        RowLayout {
            BackButton {
                onClicked: root.close()
            }

            Text {
                Layout.alignment: Qt.AlignCenter
                color: "white"
                font.pixelSize: 22
                text: "id: " + instanceId
            }


            RefreshButton {
                id: refreshButton
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    aws.describe_instance(instanceId)
                }
                Connections {
                    target: aws
                    onDescribeStarted: {
                        if (id != instanceId) {
                            return;
                        }

                        refreshButton.startAnimation()
                        console.log("Describe started")
                    }
                    onDescribeFinished: {
                        if (id != instanceId) {
                            return;
                        }
                        showDetails()
                        refreshButton.stopAnimation()
                        console.log("Describe finished")
                    }
                }
            }
        }

        Rectangle {
            border.color: Style.separatorColor
            color: "transparent"
            Layout.fillWidth: true
            Layout.preferredHeight: childrenRect.height
            RowLayout {
                DeleteButton {
                    id: deleteButton
                    onClicked: {
                        console.log("Delete " + instanceId)
                        aws.terminate_instance(instanceId)
                    }
                    Connections {
                        target: aws
                        onTerminateStarted: {
                            if (id != instanceId) {
                                return;
                            }

                            console.log("Terminate started")
                            deleteButton.startAnimation()
                        }
                        onTerminateFinished: {
                            if (id != instanceId) {
                                return;
                            }

                            console.log("Terminate finished")
                            deleteButton.stopAnimation()
                            root.close()
                        }
                    }
                }

                TestSshButton {
                    id: sshCheckButton
                    color: sshAvailable ? Style.accentColor : Style.separatorColor
                    onClicked: {
                        console.log("Test ssh " + instanceId)
                        aws.check_instance_ssh(instanceId)
                    }
                    Connections {
                        target: aws
                        onSshCheckStarted: {
                            if (id != instanceId) {
                                return;
                            }
                            console.log("Test ssh started")
                            sshCheckButton.startAnimation()
                        }
                        onSshCheckFinished: {
                            if (id != instanceId) {
                                return;
                            }
                            console.log("Test ssh finished")
                            sshCheckButton.stopAnimation()
                        }
                    }
                }

                SshCommandButton {
                    id: sshCommandButton
                    color: !sshAvailable ? Style.separatorColor : Style.accentColor
                    enabled: sshAvailable
                    Connections {
                        target: aws

                        onRunCommandStarted: {
                            if (id != instanceId) {
                                return;
                            }

                            showSshConsole()
                            console.log("SSh cmd started")
                        }

                        onRunCommandFinished: {
                            if (id != instanceId) {
                                return;
                            }
                            consoleOutput = output
                            console.log("Ssh cmd finished")
                        }
                    }
                    onClicked:  {
                        aws.run_command_on_instance(instanceId, "uname -a")
                    }
                }

                CopyArchiveButton {
                    id: copyCommandButton
                    color: !sshAvailable ? Style.separatorColor : Style.accentColor
                    enabled: sshAvailable
                    Connections {
                        target: aws

                        onCopyStarted: {
                            if (id != instanceId) {
                                return;
                            }
                            console.log("Copy archive started")
                        }

                        onCopyFinished: {
                            if (id != instanceId) {
                                return;
                            }
                            console.log("Copy archive finished")
                        }
                    }
                    onClicked:  {
                        aws.copy_files_to_instance(instanceId)
                    }
                }
            }
        }

        Loader {
            id: loader
            Layout.fillWidth: true
            Layout.fillHeight: true
            property string currentItem: currentView
            sourceComponent: {
                switch (currentItem) {
                    case "sshConsole":
                        return sshConsole
                    case "details":
                        return detailsList
                }
                return detailsList
            }

            Component {
                id: detailsList
                InstanceDetailsList {
                    description: root.description
                }
            }
            Component {
                id: sshConsole
                ColumnLayout {
                    Rectangle {
                        Layout.fillWidth: true
                        Layout.preferredHeight: 24
                        color: "transparent"
                        border.color: Style.separatorColor
                        RowLayout {
                            anchors.fill: parent
                            Text {
                                font.bold: true
                                Layout.alignment: Qt.AlignVCenter
                                text: instanceId + " > "
                                font.family: "Monaco"
                                font.pixelSize: 12
                                color: Style.accentColor
                            }
                            TextEdit {
                                id: cmdLine
                                font.pixelSize: 12
                                Layout.fillWidth: true
                                Layout.alignment: Qt.AlignVCenter
                                color: Style.secondaryTextColor
                                text: "uname - a"
                                Keys.onPressed: {
                                    if (event.key !== Qt.Key_Enter && event.key !== Qt.Key_Return) {
                                        return;
                                    }
                                    text = text.trim()
                                    aws.run_command_on_instance(instanceId, text)
                                    event.accepted = true
                                    cursorPosition = 0
                                    selectAll()
                                }
                            }
                        }
                    }
                    Flickable {
                        Layout.fillWidth: true
                        Layout.fillHeight: true

                        contentWidth: width
                        contentHeight: textEdit.height
                        clip: true
                        TextEdit {
                            id: textEdit
                            readOnly: true
                            font.family: "Monaco"
                            font.pixelSize: 10
                            selectByMouse: true
                            color: Qt.darker("white")
                            text: root.consoleOutput.join("\n")
                            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        }
                    }
                }
            }
        }
    }
}
