import QtQuick 2.12
import QtQuick.Controls 2.12

import ".."
Button {
    id: root
    background: Rectangle {
        color: "transparent"
        border.color: Style.separatorColor
    }

    icon {
        source: "qrc:/images/back.png"
        color: Style.accentColor
        height: 20
        width: 20
        name: "back"
    }
}
