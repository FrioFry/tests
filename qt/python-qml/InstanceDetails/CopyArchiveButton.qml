import QtQuick 2.12
import QtQuick.Controls 2.12

Button {
    id: refreshButton
    property color color: Style.separatorColor
    background: Item {}

    function startAnimation() {
        rotateAnimation.running = true
    }

    function stopAnimation() {
        rotateAnimation.running = false
        refreshButton.rotation = 0
    }

    onVisibleChanged:  {
        if (visible) {
            stopAnimation()
        }
    }

    icon {
        source: "qrc:/images/archive.png"
        color: refreshButton.color
        height: 32
        width: 32
        name: "name"
    }

    RotationAnimator on rotation {
        id: rotateAnimation
        running: false
        from: 0;
        to: 360;
        duration: 600;
        loops: Animation.Infinite
    }
}
