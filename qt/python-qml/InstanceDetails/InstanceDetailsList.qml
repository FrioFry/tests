import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import ".."
ListView {
    property var description: ({})
    property var descriptionKeys: ['ImageId', 'InstanceId', 'InstanceType', 'KeyName', 'LaunchTime', 'Placement', 'PublicIpAddress', 'SecurityGroups', 'SubnetId', 'VpcId', 'Tags', 'State' ]
    property var intersection: (Object.keys(description)).filter(function(n) {
        return descriptionKeys.indexOf(n) !== -1;
    });
    model: intersection
    spacing: 12
    delegate: RowLayout {
        anchors {
            left: parent.left
            right: parent.right
        }
        height: 16
        TextEdit {
            Layout.preferredWidth: 120
            color: Style.secondaryTextColor
            font.pixelSize: 12
            cursorVisible: false
            readOnly: true
            selectByMouse: true
            text: modelData
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }

        TextEdit {
            color: "white"
            font.pixelSize: 12
            cursorVisible: false
            readOnly: true
            selectByMouse: true
            text: description[modelData]
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }

        Item {
            Layout.fillWidth: true
        }
    }
}
