pragma Singleton
import QtQuick 2.8

QtObject {
    readonly property color separatorColor: "#373F46"
    readonly property color primaryBackgroundColor: "#1D2226"
    readonly property color secondaryTextColor: "#B2B8BD"
    readonly property color accentColor: "#2196F3"
}
