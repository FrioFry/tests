import boto3
from botocore.exceptions import ClientError, WaiterError
import json
import uuid
import sys

_cli_input_config = "cli-input.json"
_regions = ["us-east-2"]
_ip_alloc = "eipalloc-0e72814c1135eb024"
_default_duration_min = 60


def prepare_cli_config(instance_type, duration):
    # prepare config
    with open(_cli_input_config, "r") as f:
        cli_config = json.load(f)
    cli_config["DryRun"] = False
    id = uuid.uuid4()
    cli_config["ClientToken"] = str(id)
    cli_config["BlockDurationMinutes"] = duration
    cli_config["LaunchSpecification"]["InstanceType"] = instance_type
    return cli_config


# request_spot_instances - with config
# returns None if failed or the first response
def try_request_instance(ec2, cli_config):
    request_id = ""
    try:
        response = ec2.request_spot_instances(**cli_config)
        # print(json.dumps(response, indent=4, default=str))
        result_ok = response["ResponseMetadata"]["HTTPStatusCode"] == 200
        responses = response.get("SpotInstanceRequests", [])
        if not result_ok or not responses:
            print("Spot instance request failed")
            return None

        request_id = responses[0]["SpotInstanceRequestId"]
        print(">>> WAITING SpotInstanceRequestId:" + request_id)
        waiter = ec2.get_waiter("spot_instance_request_fulfilled")
        waiter.config.max_attempts = 2
        waiter.wait(SpotInstanceRequestIds=[request_id])
        print("Request fulfilLed")
        return request_id
    except WaiterError:
        print("Wait failed")
        try_terminate(request_id, None, ec2.meta.region_name)
    return None


# describe_spot_instance_requests and get instance id
# returns instance_id
def get_instance_id(ec2, request_id):
    print(">>> DESCRIBING REQUEST {}".format(request_id))
    response = ec2.describe_spot_instance_requests(SpotInstanceRequestIds=[request_id])
    # print(json.dumps(response, indent=4, default=str))
    result_ok = response["ResponseMetadata"]["HTTPStatusCode"] == 200
    if not result_ok or "SpotInstanceRequests" not in response:
        print("Spot instance describe request failed, status code: " + result_code)
        return None

    req0 = response["SpotInstanceRequests"][0]
    instance_id = req0["InstanceId"]
    price = req0["ActualBlockHourlyPrice"]
    print("InstanceId={} for {}$".format(instance_id, price))
    return instance_id


# assign predefined IP to instance
def assign_ip(ec2, ipalloc, instance_id):
    response = ec2.associate_address(
        AllocationId=ipalloc,
        InstanceId=instance_id,
        AllowReassociation=True)


# cancels Spot request, terminates instance
def try_terminate(request_id, instance_id, region_name):
    print("TERMINATING request_id={}, instance_id={}".format(request_id, instance_id))
    ec2 = boto3.client("ec2")
    if instance_id:
        ec2r = boto3.resource("ec2")
        ec2r.instances.filter(InstanceIds=[instance_id]).terminate()
    if request_id:
        ec2.cancel_spot_instance_requests(SpotInstanceRequestIds=[request_id])


# list zones in region
def list_region_zones(ec2, region):
    try:
        response = ec2.describe_availability_zones(Filters=[{"Name": "region-name", "Values": [region]}])
        # print(json.dumps(response, indent=4, default=str))
        result = [z["ZoneName"] for z in response["AvailabilityZones"]]
        # result.reverse()
        return result
    except:
        print("Failed to get zone names {}".format(region))
    return []


# request spot instance in several zones
# @param zones - list of zones
# returns response
def try_request_instance_in_zones(cli_config, regions, ipalloc):
    instance_id = ""
    request_id = ""
    region = ""
    try:
        for region in regions:
            ec2 = boto3.client("ec2", region_name=region)
            zones = list_region_zones(ec2, region)
            print("Trying region: {} with zones {}".format(region, zones) )
            for zone in zones:
                cli_config["LaunchSpecification"]["Placement"]["AvailabilityZone"] = zone
                id = uuid.uuid4()
                cli_config["ClientToken"] = str(id)
                print(">>> REQUESTING SPOT INSTANCE in {}".format(zone))
                request_id = try_request_instance(ec2, cli_config)
                if not request_id:
                    continue
                instance_id = get_instance_id(ec2, request_id)
                if not instance_id:
                    continue
                assign_ip(ec2, ipalloc, instance_id)
                print("Success")
                return request_id, instance_id, region
    except ClientError as e:
        print("Unexpected error: %s" % e)
        try_terminate(request_id, instance_id, region)
    return request_id, instance_id, region


def request_instance(instance_type, duration, ipalloc):
    cli_config = prepare_cli_config(instance_type, duration)

    result = try_request_instance_in_zones(cli_config, _regions, ipalloc)
    request_id, instance_id, region = result
    try_terminate(request_id, instance_id, region)


def main():
    instance_type = "g3s.xlarge"    # "p2.xlarge", "p3.2xlarge
    if len(sys.argv) > 1:
        instance_type = sys.argv[1]
    request_instance(instance_type, _default_duration_min, _ip_alloc)   # 0.356 $


if __name__ == "__main__":
    main()
