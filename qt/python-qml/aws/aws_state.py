import os, sys, json, traceback
from datetime import datetime, timezone
import boto3
import humanize

from PySide2.QtCore import QObject, Signal, Slot, Property
from PySide2.QtCore import QRunnable, QThreadPool

from .request_aws import get_instances, describe_instance, \
    aws_terminate_instance, aws_create_instance, aws_check_ssh, \
    aws_run_command, aws_wait_instance_checked, aws_copy_to_instance

from .worker import Worker

class AwsState(QObject):
    def __init__(self, *args, **kwargs):
        QObject.__init__(self)
        self.__running_instances = []
        self.__instance_description = {}
        self.ec2Resource = boto3.resource("ec2")
        self.ec2Client = boto3.client("ec2")
        self.threadpool = QThreadPool()
        self.user = "ubuntu"
        self.key_file = "/Users/ricko/.ssh/cuda-worker.pem"
        self.ssh_timeout = 5
        self.ssh_available = {}
        self.instance_types = {
            "t2.nano": "lt-0539add7904d3094a",
            "g3s.xlarge": "lt-017d8987bb6a553a9",
            "p2.xlarge": "lt-0287d7b67a507b5ab"
        }
        print("Multithreading with maximum %d threads" % self.threadpool.maxThreadCount())


    ### Running Instances
    runningInstancesChanged = Signal()
    refreshStarted = Signal()
    refreshFinished = Signal()

    def __set_running_instances(self, value):
        if self.__running_instances != value:
            self.__running_instances = value
            self.runningInstancesChanged.emit()
        self.refreshFinished.emit()

    @Slot(result="QVariantList")
    def get_running_instances(self):
        return self.__running_instances

    @Slot()
    def refresh(self):
        self.refreshStarted.emit()
        worker = Worker(get_instances, self.ec2Resource)
        worker.signals.result.connect(self.__set_running_instances)
        self.threadpool.start(worker)

    runningInstances = Property("QVariantList", get_running_instances, notify=runningInstancesChanged)


    ### Describe Instance
    describeStarted = Signal(str, arguments=['id'])
    describeFinished = Signal(str, arguments=['id'])
    instanceDescriptionChanged = Signal()

    def __set_instance_description(self, id, value):
        if self.__instance_description != value:
            self.__instance_description = value
            self.instanceDescriptionChanged.emit()
        self.describeFinished.emit(id)

    @Slot(str)
    def describe_instance(self, id):
        self.ssh_available[id] = False
        self.describeStarted.emit(id)
        worker = Worker(describe_instance, self.ec2Client, id)
        def on_finish(value):
            self.__set_instance_description(id, value)
        worker.signals.result.connect(on_finish)
        self.threadpool.start(worker)

    @Slot(result="QVariantMap")
    def get_instance_description(self):
        return self.__instance_description

    instanceDescription = Property("QVariantMap", get_instance_description, notify=instanceDescriptionChanged)


    ### Terminate
    terminateStarted = Signal(str, arguments=['id'])
    terminateFinished = Signal(str, arguments=['id'])

    @Slot(str)
    def terminate_instance(self, id):
        self.terminateStarted.emit(id)
        worker = Worker(aws_terminate_instance, self.ec2Resource, self.ec2Client, id)

        def on_finished(instance_id):
            self.terminateFinished.emit(instance_id)
        worker.signals.result.connect(on_finished)
        self.threadpool.start(worker)


    ### Terminate
    createStarted = Signal()
    statusCheckStarted = Signal(str, arguments=['id'])
    createFinished = Signal(str, arguments=['id'])

    @Slot(str)
    def create_instance(self, instanceType):
        self.createStarted.emit()
        templateId = self.instance_types.get(instanceType, self.instance_types["t2.nano"])

        def on_finished(instance_id):
            if instance_id:
                self.statusCheckStarted.emit(instance_id)
                worker1 = Worker(aws_wait_instance_checked, self.ec2Client, instance_id)

                def on_finished2(instance_id):
                    self.createFinished.emit(instance_id)
                worker1.signals.result.connect(on_finished2)
                self.threadpool.start(worker1)
            else:
                self.createFinished(instance_id)

        worker = Worker(aws_create_instance, self.ec2Client, templateId)
        worker.signals.result.connect(on_finished)
        self.threadpool.start(worker)


    ### Test ssh
    sshCheckStarted = Signal(str, arguments=['id'])
    sshCheckFinished = Signal(str, arguments=['id'])
    instanceSshAvailableChanged = Signal()

    @Slot(str)
    def check_instance_ssh(self, id):
        self.sshCheckStarted.emit(id)
        worker = Worker(aws_check_ssh, self.ec2Client, id, self.user, self.key_file, self.ssh_timeout)
        def on_finished(value):
            if self.ssh_available.get(id, False) != value:
                self.ssh_available[id] = value
                self.instanceSshAvailableChanged.emit()
            self.sshCheckFinished.emit(id)

        worker.signals.result.connect(on_finished)
        self.threadpool.start(worker)

    @Slot(result="QVariantMap")
    def get_ssh_available(self):
        return self.ssh_available

    instanceSshAvailable = Property("QVariantMap", get_ssh_available, notify=instanceSshAvailableChanged)


    ### Run command
    runCommandStarted = Signal(str, arguments=['id'])
    runCommandFinished = Signal(str, "QVariantList", arguments=['id', 'output'])

    @Slot(str, str)
    def run_command_on_instance(self, id, cmd):
        self.runCommandStarted.emit(id)
        worker = Worker(aws_run_command, self.ec2Client, id, self.user, self.key_file, cmd, self.ssh_timeout)
        def on_finished(logs):
            log_lines = logs.lstrip("b'").rstrip("'")
            log_lines = log_lines.split("\\n")
            log_lines = [l.rstrip("\\n") for l in log_lines]
            self.runCommandFinished.emit(id, log_lines)
        worker.signals.result.connect(on_finished)
        self.threadpool.start(worker)


    ### Instance types
    instanceTypesChanged = Signal()

    def _get_instance_types(self):
        return list(self.instance_types.keys())

    instanceTypes = Property("QVariantList", _get_instance_types, notify=instanceTypesChanged)


    ### Copy file to server
    copyStarted = Signal(str, arguments=['id'])
    copyFinished = Signal(str, arguments=['id'])

    @Slot(str)
    def copy_files_to_instance(self, id):
        self.copyStarted.emit(id)
        file_path = "/Users/ricko/tests/qt/python-qml/cpu_test.zip"
        dst_path = "/home/ubuntu/"
        worker = Worker(aws_copy_to_instance, self.ec2Client, id, self.user, self.key_file, file_path, dst_path, self.ssh_timeout)
        def on_finished1(id):
            self.copyFinished.emit(id)
        worker.signals.result.connect(on_finished1)
        self.threadpool.start(worker)

#rm -rf cpu_test.zip cpu_test_dist
#sudo apt-get update; sudo apt-get install -y build-essential;
#unzip cpu_test.zip -d .; cd ~/cpu_test_dist/cpu_test/; ./aws_run.sh

