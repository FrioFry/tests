import boto3
from datetime import datetime, timezone
import humanize
from botocore.exceptions import ClientError, WaiterError
import paramiko
from paramiko import BadHostKeyException, AuthenticationException,SSHException
import socket
import json
import uuid
import time
import os


_region = "us-east-2"
_template_id = "lt-0d5e39dd1df265a69" # "lt-017d8987bb6a553a9"
_id_to_ip = {}
_ssh_clients = {}


def get_instances(ec2):
    filters = [
#        {
#            'Name': 'instance-state-name',
#            'Values': ['running']
#        }
    ]
    running_instances = []
    instances = ec2.instances.filter(Filters=filters)

    for instance in instances:
        # for each instance, append to array and print instance id
        duration = datetime.now(timezone.utc) - instance.launch_time
        durationStr = humanize.naturaltime(duration)
        running_instances.append({
            "id": instance.id,
            "state": instance.state["Name"],
            "duration": durationStr
        })
    return running_instances


def describe_instance(ec2, instance_id):
    response = ec2.describe_instances(InstanceIds=[instance_id])
    info = response['Reservations'][0]['Instances'][0]
    result = {}
    for k in info:
        result[k] = str(info[k])
    return result


def aws_terminate_instance(ec2_resource, ec2_client, instance_id):
    ec2_resource.instances.filter(InstanceIds=[instance_id]).terminate()
    waiter = ec2_client.get_waiter("instance_terminated")
    waiter.wait(InstanceIds=[instance_id])
    return instance_id


def aws_create_instance(ec2, template_id):
    response = ec2.run_instances(LaunchTemplate={"LaunchTemplateId": template_id},
                                 MinCount=1,
                                 MaxCount=1,
                                 #SecurityGroupIds=['sg-7a334318'])
                                 #SubnetId='subnet-50eef538'
                                 )
    instance_id = get_instance_id(response)
    return instance_id


def aws_wait_instance_checked(ec2, instance_id):
    waiter = ec2.get_waiter("system_status_ok")
    waiter.wait(InstanceIds=[instance_id])
    return instance_id


def aws_prepare_ssh(ec2, id, user, key_file, timeout_sec=None):
    description = describe_instance(ec2, id)
    ip = description['PublicIpAddress']
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ip, username=user, key_filename=key_file, timeout=timeout_sec)
    return ssh


def aws_check_ssh(ec2, id, user, key_file, timeout_sec=None):
    ssh = None
    try:
        ssh = aws_prepare_ssh(ec2, id, user, key_file, timeout_sec)
        return True
    except (BadHostKeyException, AuthenticationException,
            SSHException, socket.error) as e:
        print(str(e))
    finally:
        if ssh:
            ssh.close()
    return False


def aws_run_command(ec2, instance_id, user, key_file, cmd, timeout_sec=30):
    ssh = None
    try:
        ssh = aws_prepare_ssh(ec2, instance_id, user, key_file, timeout_sec)
        _, stdout, _ = ssh.exec_command(cmd)
        logs = str(stdout.read())
        return logs
    except (BadHostKeyException, AuthenticationException,
            SSHException, socket.error) as e:
        print(str(e))
    finally:
        if ssh:
            ssh.close()
    return None


def aws_copy_to_instance(ec2, instance_id, user, key_file, file_path, dst_dir, timeout_sec=30):
    ssh = None
    try:
        ssh = aws_prepare_ssh(ec2, instance_id, user, key_file, timeout_sec)
        with ssh.open_sftp() as sftp:
            filename = os.path.basename(file_path)
            try:
                sftp.mkdir(dst_dir)
                sftp.chdir(dst_dir)
                sftp.remove(filename)
            except:
                pass
            print(os.path.exists(file_path))
            print(file_path)
            sftp.put(file_path, filename)

    except (BadHostKeyException, AuthenticationException,
            SSHException, socket.error) as e:
        print(str(e))
    finally:
        if ssh:
            ssh.close()
    return instance_id

####


def request_status(response):
    return response['ResponseMetadata']['HTTPStatusCode']


def get_instance_id(response):
    instances = response['Instances']
    if request_status(response) == 200 and len(instances):
        return instances[0]['InstanceId']
    return None


def terminate_instance(instance_id):
    ec2r = boto3.resource("ec2")
    print("Terminating...")
    ec2r.instances.filter(InstanceIds=[instance_id]).terminate()
    ec2 = boto3.client("ec2", _region)
    waiter = ec2.get_waiter("instance_terminated")
    waiter.wait(InstanceIds=[instance_id])
    print("Terminated")



def get_ec2():
    global _ec2
    if not _ec2:
        print("init ec2")
        _ec2 = boto3.client("ec2", _region)
    return _ec2


def request_instance():
    print("create ec2")
    ec2 = get_ec2()
    print("run_instances")
    try:
        response = ec2.run_instances(LaunchTemplate={"LaunchTemplateId": _template_id},
                                     MinCount=1,
                                     MaxCount=1,
                                     #SecurityGroupIds=['sg-7a334318'])
                                     #SubnetId='subnet-50eef538'
                                     )
        print("Response:\n{}".format(response))
    except Exception as e:
        print("failed {}".format(e))
    instance_id = get_instance_id(response)
    if instance_id:
        waiter = ec2.get_waiter("instance_running")
        waiter.wait(InstanceIds=[instance_id])
        print("Instance ready")
        id = get_instance_id(response)
        return id
    return None


def wait_for_eof(stdout):
    timeout = 30
    endtime = time.time() + timeout
    while not stdout.channel.eof_received:
        time.sleep(1)
        if time.time() > endtime:
            stdout.channel.close()
            break
    stdout.read()


def open_ssh(instance_id):
    global _ssh_clients
    if instance_id not in _ssh_clients:
        instance_ip = get_public_ip4(instance_id)
        key = paramiko.RSAKey.from_private_key_file("../cuda-worker.pem")
        print("SSHClient")
        client = paramiko.SSHClient()
        print("set_missing_host_key_policy")
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        _ssh_clients[instance_id] = client
    return _ssh_clients.get(instance_id)


def run_command(instance_id, cmd, attempts=50, timeout=10):
    client = open_ssh(instance_id)
    for i in range(attempts):
        try:
            print("connecting {}".format(i))
            client.connect(hostname=instance_ip,
                           username="ubuntu",
                           pkey=key,
                           timeout=timeout,
                           banner_timeout=timeout,
                           auth_timeout=timeout)
            _, stdout, _ = client.exec_command(cmd)
            print(stdout.read())
            wait_for_eof(stdout)
            return True
        except Exception as e:
            print("Failed {}".format(str(e)))
            time.sleep(timeout)
    return False


def copy_to_instance(instance_id, file_path):
    client = open_ssh(instance_id)
    try:
        with client.open_sftp() as sftp:
            filename = os.path.basename(file_path)
            sftp.put(file_path, "~/{}".format(filename))
    except Exception as e:
        print(e)
