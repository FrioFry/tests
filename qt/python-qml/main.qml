import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "InstanceDetails"
import "InstanceList"


ApplicationWindow {
    visible: true
    width: 400
    height: 600

    Component.onCompleted: {
        aws.refresh()
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: 0
        interactive: false
        InstanceList {
            visible: swipeView.currentIndex == 0
            onOpenDetails: {
                details.instanceId = instanceId
                swipeView.currentIndex = 1
            }
        }
        InstanceDetails {
            id: details
            visible: swipeView.currentIndex == 1
            onClose: {
                swipeView.currentIndex = 0
            }
        }
    }

}
