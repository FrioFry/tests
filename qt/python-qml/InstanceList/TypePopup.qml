import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import ".."
Popup {
    id: popup
    property var model: ["t2.nano", "g3s.xlarge", "p2.xlarge"]
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
    signal selected(string name)

    background: Rectangle {
        radius: 4
        color: Style.primaryBackgroundColor
    }

    contentItem: ColumnLayout {
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
            topMargin: 8
        }

        Repeater {
            model: popup.model
            delegate: Button {
                Layout.alignment: Qt.AlignCenter
                Layout.preferredWidth: 120
                text: modelData
                onClicked: {
                    selected(modelData)
                }
            }
        }
    }
}
