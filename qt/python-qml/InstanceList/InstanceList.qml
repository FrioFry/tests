import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

import ".."

Item {
    id: root
    property int runningCount: aws.runningInstances.length
    signal openDetails(string instanceId)

    onVisibleChanged:  {
        if (visible) {
            aws.refresh()
        }
    }

    Timer {
        id: refreshTimer
        interval: 10000
        repeat: true
        running: visible
        onTriggered: {
            aws.refresh()
        }
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 32

        spacing: 16


        Text {
            text: "Aws helper"
            Layout.alignment: Qt.AlignCenter
            color: "white"
            font {
                pixelSize: 22
            }
        }

        RowLayout {
            Layout.fillWidth: true
            spacing: 12
            Text {
                font.pixelSize: 18
                color: "white"
                text: "Running instances: " + root.runningCount
            }

            Item {
                Layout.fillWidth: true
            }

            RefreshButton {
                id: refreshButton
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    aws.refresh()
                }
                Connections {
                    target: aws
                    onRefreshFinished: {
                        refreshButton.stopAnimation()
                        console.log("Refresh finished")
                    }
                    onRefreshStarted: {
                        refreshButton.startAnimation()
                        console.log("Refresh started")
                    }
                }
            }
        }

        Rectangle {
            border.color: Style.separatorColor
            color: "transparent"
            Layout.fillWidth: true
            Layout.preferredHeight: childrenRect.height
            RowLayout {
                PlayButton {
                    id: playButton
                    onClicked: {
                        selectTypePopup.open()
                    }
                    Connections {
                        target: aws
                        onCreateStarted: {
                            console.log("Create started")
                            playButton.startAnimation()
                            aws.refresh()
                            playButton.enabled = false
                        }
                        onStatusCheckStarted: {
                            console.log("Check started")
                        }
                        onCreateFinished: {
                            playButton.enabled = true
                            console.log("Create finished")
                            playButton.stopAnimation()
                            aws.refresh()
                        }
                    }
                }
            }

            TypePopup {
                id: selectTypePopup
                x: (parent.width - width)/2
                y: 0
                model: aws.instanceTypes
                width: parent.width
                onSelected: {
                    console.log("Creating " + name)
                    aws.create_instance(name)
                    close()
                }
            }
        }

        Rectangle {
            height: 1
            Layout.fillWidth: true
            color: Style.separatorColor
        }

        InstanceListView {
            model: aws.runningInstances
            onOpenDetails: {
                root.openDetails(instanceId)
            }
        }

        Item {
            Layout.fillHeight: true
        }
    }
}

