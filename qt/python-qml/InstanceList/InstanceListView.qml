import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

import ".."
ListView {
    Layout.fillWidth: true
    Layout.fillHeight: true

    anchors.margins: 4
    signal openDetails(string instanceId)

    delegate: Rectangle {
        color: mouseArea.containsMouse ? Style.primaryBackgroundColor : "transparent"
        radius: 4
        height: 22
        width: parent.width
        RowLayout {
            anchors.fill: parent
            Text {
                Layout.alignment: Qt.AlignVCenter
                font.pixelSize: 16
                text: modelData["id"]
                color: "white"
            }
            Text {
                Layout.alignment: Qt.AlignVCenter
                font.pixelSize: 12
                text: " - " + modelData["state"]
                color: Style.secondaryTextColor
            }
            Text {
                Layout.alignment: Qt.AlignVCenter
                font.pixelSize: 12
                text: " " + modelData["duration"]
                color: Style.secondaryTextColor
            }
            Item {
                Layout.fillWidth: true
            }
        }

        Rectangle {
            height: 1
            anchors {
                left: parent.left
                right: parent.right
                top: parent.bottom
            }
            color: Style.separatorColor
        }

        MouseArea {
            id: mouseArea
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                openDetails(modelData["id"])
            }
        }
    }
}
