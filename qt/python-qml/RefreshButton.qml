import QtQuick 2.12
import QtQuick.Controls 2.12

Button {
    id: refreshButton
    background: Item {}

    function startAnimation() {
        rotateAnimation.running = true
    }

    function stopAnimation() {
        rotateAnimation.running = false
        refreshButton.rotation = 0
    }
    onVisibleChanged:  {
        if (visible) {
            stopAnimation()
        }
    }

    icon {
        source: "qrc:/images/refresh.png"
        color: Style.accentColor
        height: 20
        width: 20
        name: "name"
    }

    RotationAnimator on rotation {
        id: rotateAnimation
        running: false
        from: 0;
        to: 360;
        duration: 600;
        loops: Animation.Infinite
    }
}
