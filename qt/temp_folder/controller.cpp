#include "controller.h"
#include <QDir>
#include <QDebug>
#include <QStandardPaths>
#include <QDesktopServices>
#include <QProcess>

namespace
{

bool copyDir(const QString &srcPath, const QString &dstPath)
{
    qDebug() << srcPath << " " << dstPath;

    QDir dstDir(dstPath);

    if (dstDir.exists()) {
        qDebug() << "exists " << dstDir.absolutePath();
    }
    if (!dstDir.mkpath(".")) {
        qDebug() << "cannot create " << dstDir.path();
        return false;
    }

    QDir srcDir(srcPath);
    qDebug() << srcDir.absolutePath() << " " << srcDir.exists();
    foreach(const QFileInfo &info, srcDir.entryInfoList(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot)) {
        qDebug() << info.fileName();
        QString srcItemPath = srcPath + "/" + info.fileName();
        QString dstItemPath = dstPath + "/" + info.fileName();
        if (info.isDir()) {
            if (!copyDir(srcItemPath, dstItemPath)) {
                return false;
            }
        } else if (info.isFile()) {
            if (!QFile::copy(srcItemPath, dstItemPath)) {
                return false;
            }
        } else {
            qDebug() << "Unhandled item" << info.filePath() << "in cpDir";
        }
    }
    return true;
}

} // namespace

Controller::Controller(QObject *parent)
    : QObject(parent)
{
}

QString Controller::folderName() const
{
    return _folderName;
}

void Controller::createTempFolder()
{
    QString tmpDir = QStandardPaths::writableLocation(QStandardPaths::TempLocation) + "/CiphrAppMaintenanceTool/1/";
    QDir maintenanceDir(tmpDir);
    maintenanceDir.removeRecursively();
    maintenanceDir.mkpath(".");

    _folderName = maintenanceDir.path();
    qDebug() << "Hi: " << _folderName;

    emit folderNameUpdated();
}

void Controller::openDir()
{
    QDesktopServices::openUrl("file:///" + _folderName);
}

void Controller::copy()
{
    QString maintenanceApp = "/Applications/CiphrApp.app/Contents/MacOS/MaintenanceTool";
    QDir f(_folderName);
    copyDir(maintenanceApp, _folderName);
}

void Controller::launch()
{
    QStringList runArguments;
    runArguments << "--updater";
    runArguments << "--verbose";
    //auto targetDir = "TargetDir=/Applications/CiphrApp.app/Contents/MacOS/MaintenanceTool";
    auto targetDir = "TargetDir=" + _folderName;
    runArguments << targetDir;

    QString maintenanceAppPath = _folderName + "/maintenancetool.app/Contents/Macos/maintenancetool";
    QFileInfo toolInfo(maintenanceAppPath);
    qDebug() << toolInfo.absoluteFilePath() << " " << runArguments;
    if (!toolInfo.exists()) {
        qWarning() << "Updater app does not exist";
        return;
    }

//    bool ok = QProcess::startDetached(toolInfo.absoluteFilePath(),
//                                      runArguments,
//                                      toolInfo.absolutePath());
//    qDebug() << ok;
}

QString Controller::updateStatus() const
{
    return _updateStatus;
}

void Controller::checkUpdates()
{
    QString maintenanceAppPath = _folderName + "/maintenancetool.app/Contents/Macos/maintenancetool";
    QFileInfo toolInfo(maintenanceAppPath);

    _mainProcess = new QProcess(this);
    _mainProcess->setProgram(toolInfo.absoluteFilePath());
    QStringList args;
    args << "--checkupdates";
    _mainProcess->setArguments(args);

    qDebug() << _mainProcess->arguments() << " " << _mainProcess->program();


    connect(_mainProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            this, &Controller::updaterReady, Qt::QueuedConnection);
    connect(_mainProcess, &QProcess::errorOccurred,
            this, &Controller::updaterError, Qt::QueuedConnection);

    _mainProcess->start(QIODevice::ReadOnly);
}

void Controller::updaterReady(int , QProcess::ExitStatus exitStatus)
{
    if(_mainProcess) {
        if(exitStatus == QProcess::NormalExit) {
            _updateStatus = _mainProcess->readAllStandardOutput();
            _mainProcess->deleteLater();
            _mainProcess = nullptr;
            emit updateStatsUpdated();
        } else
            updaterError(QProcess::Crashed);
    }
}

void Controller::updaterError(QProcess::ProcessError )
{
    if (_mainProcess) {
        _mainProcess->deleteLater();
        _mainProcess = nullptr;
    }
    qDebug() <<"Failed";
}
