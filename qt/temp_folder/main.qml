import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Layouts 1.13
import QtQuick.Controls 2.12

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
    ColumnLayout  {
        anchors.fill: parent
        spacing: 20
        Button {
            Layout.alignment: Qt.AlignTop | Qt.AlignHCenter
            Layout.topMargin: 100
            text: "Create temp folder"
            onClicked: {
                controller.createTempFolder()
            }
        }
        Button {
            Layout.alignment: Qt.AlignTop| Qt.AlignHCenter
            text: "Open dir"
            visible: controller.folderName
            onClicked: {
                controller.openDir()
            }
        }
        Button {
            Layout.alignment: Qt.AlignTop| Qt.AlignHCenter
            text: "Copy dir"
            visible: controller.folderName
            onClicked: {
                controller.copy()
            }
        }
        Button {
            Layout.alignment: Qt.AlignTop| Qt.AlignHCenter
            text: "Launch updater"
            visible: controller.folderName
            onClicked: {
                controller.launch()
            }
        }
        Button {
            Layout.alignment: Qt.AlignTop| Qt.AlignHCenter
            text: "Check Updates"
            visible: controller.folderName
            onClicked: {
                controller.checkUpdates()
            }
        }

        Text {
            Layout.alignment: Qt.AlignTop
            text: controller.folderName
        }
        Text {
            Layout.alignment: Qt.AlignTop
            text: controller.updateStatus
        }
    }
}
