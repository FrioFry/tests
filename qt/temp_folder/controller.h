#ifndef Controller_HPP
#define Controller_HPP

#include <QObject>
#include <QVariant>
#include <QUrl>
#include <QProcess>

class NotesService;


class Controller : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString folderName READ folderName NOTIFY folderNameUpdated)
    Q_PROPERTY(QString updateStatus READ updateStatus NOTIFY updateStatsUpdated)

public:
    explicit Controller(QObject *parent = nullptr);
    QString folderName() const;
    QString updateStatus() const;

    Q_INVOKABLE void createTempFolder();
    Q_INVOKABLE void openDir();
    Q_INVOKABLE void copy();
    Q_INVOKABLE void launch();
    Q_INVOKABLE void checkUpdates();
public slots:
    void updaterReady(int exitCode, QProcess::ExitStatus exitStatus);
    void updaterError(QProcess::ProcessError error);
signals:
    void folderNameUpdated();
    void updateStatsUpdated();

private:
    QString _folderName;
    QString _updateStatus;
    QProcess *_mainProcess {nullptr};
};

Q_DECLARE_METATYPE(Controller *)

#endif // Controller_HPP
