#include <QCoreApplication>
#include <QHash>
#include <QSharedPointer>
#include <QDebug>

struct VaultFolder {
    int id { -1 };
};
typedef QSharedPointer<VaultFolder> VaultFolderPtr;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QHash<int, VaultFolderPtr> foldersMap;
    auto x = foldersMap.value(0);
    auto x1 = foldersMap.value(1);
    qDebug() << x.isNull();
    qDebug() << x1.isNull();
    qDebug() << x->id;
    qDebug() << x1->id;
    return a.exec();
}
