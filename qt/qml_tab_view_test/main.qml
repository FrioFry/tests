import QtQuick 2.10
import QtQuick.Window 2.3
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3

Rectangle {
    width: 1200
    height: 800
    x: 50
    y: 50
    color: "white"
    TabView {
        id: frame
        anchors.fill: parent
        anchors.margins: 4
        Tab { title: "Tab 1"
            Rectangle {
                color: "lightblue"
                Text {
                    text: title
                }
            }
        }
        Tab { title: "Tab 2" }
        Tab { title: "Tab 3" }

        style: TabViewStyle {
            frameOverlap: -12
            tab: Rectangle {
                color: styleData.selected ? "steelblue" :"lightsteelblue"
                border.color:  "steelblue"
                implicitWidth: Math.max(text.width + 4, 80)
                implicitHeight: 20
                radius: 2
                Text {
                    id: text
                    anchors.centerIn: parent
                    text: styleData.title
                    color: styleData.selected ? "white" : "black"
                }
            }
            tabsAlignment: Qt.AlignHCenter
            tabBar: Rectangle {
                color: "yellow"
                width: 600
                Text {
                    text: "tabbar"
                }
            }
            tabsMovable: true
            frame: Rectangle {
                color: "steelblue"
            }
            leftCorner: Rectangle {
                width: 10
                height: 10
                color: "black"
                border.color: "red"
                border.width: 2
            }
        }
    }

}
