import QtQuick 2.10
import QtQuick.Window 2.3
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Rectangle {
    property color rectColor
    width: 200
    height: 200
    color: rectColor ? rectColor : "blue"
}
