#include <QGuiApplication>

#include <QtQuick/QQuickView>


int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);

    QQuickView *view = new QQuickView;
    //view.setResizeMode(QQuickView::SizeRootObjectToView);
    view->setSource(QUrl::fromLocalFile("main.qml"));
    view->show();

    return app.exec();
}
