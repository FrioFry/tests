#
# local conf example
#

# sessions encryption key
SECRET_KEY = '<some random value here'

DEBUG = True

DATABASES = {
  'default': {
      'ENGINE'   : 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
      'NAME'     : 'provisioning_db',                       # Or path to database file if using sqlite3.
      'USER'     : 'provisioning',                       # Not used with sqlite3.
      'PASSWORD' : '5xvEuaTzTEqpF7BE/1DT',                       # Not used with sqlite3.
      'HOST'     : 'provbox',
      'PORT'     : '',                                       # Set to empty string for default. Not used with sqlite3.
  }
}

# LFD DNS suffix
AUTO_ORIGIN_HOST_SUFFIX = '.auto.XXX.example.com'

# ITM DNS suffix, should be empty on development environments
ITM_HOSTNAME = '.itm.example.com'

# Reporting settings, S3 storage account
SWIFTSERVE_LOGS_S3_BUCKET = 'XXX.swiftserve'
SWIFTSERVE_LOGS_S3_FOLDER = 'logs'
SWIFTSERVE_LOGS_S3_ACCESSKEY = 'YYY'
SWIFTSERVE_LOGS_S3_SECRETKEY = 'ZZZ'
# s3.amazonaws.com is ok for buckets in the US standard region, but
# in general you need s3-aws-region.amazonaws.com
SWIFTSERVE_LOGS_S3_HOST = 's3.amazonaws.com'

# GeoIP settings S3 account
SWIFTSERVE_GEOIP_S3_BUCKET = 'XXXX.swiftserve'
SWIFTSERVE_GEOIP_S3_FOLDER = 'geoip'
SWIFTSERVE_GEOIP_S3_ACCESSKEY = 'YYY'
SWIFTSERVE_GEOIP_S3_SECRETKEY = 'ZZZ'
# s3.amazonaws.com is ok for buckets in the US standard region, but
# in general you need s3-aws-region.amazonaws.com
SWIFTSERVE_GEOIP_S3_HOST = 's3.amazonaws.com'

# list of people to notify about server errors
ADMINS = (
    ('alerts', 'dev-alerts@example.com'),
)
# 'From' address to put on those emails
SERVER_EMAIL = 'root@provisioning.environment.swiftserve.com'

# Sentry configuration
RAVEN_CONFIG = {
    'dsn': None,
    #'dsn': 'https://XXX:YYY@sentry1.sys.swiftserve.com/NNN',
}

# Spoiler settings
IDENTIFY = {
    'color'        : '#ffa500',
    'platform'     : 'Configure me in /usr/local/provisioning_server/settings_local.py',
    'save_message' : 'Misconfigured system.',
}

# Analytics parameters used by billing report
ANALYTICS_HOST = 'analytics.xxx.swiftserve.com'
ANALYTICS_USERNAME = 'analytics-api-user'
ANALYTICS_PASSWORD = 'analytics-secret-password'

# Billing report traffic checks. If either "minimum bytes" or "threshold"
# value is exceeded, it will be recorded as a discrepancy.
BILLING_TRAFFIC_MISMATCH_MIN_BYTES = 1000 * 1000 * 1000
# Threshold - the diff ratio between the sources is calculated as:
#   abs(source1-source2)/max(source1, source2)
BILLING_TRAFFIC_MISMATCH_THRESHOLD = 0.01
# Max number of mismatches to report - an integer or "None" for no limit.
BILLING_TRAFFIC_MISMATCH_LIMIT = None

# Timeout in seconds for all external API calls
BILLING_EXTERNAL_API_TIMEOUT = 900  # Analytics geotraffic API takes long time

# Billing report upload parameters
BILLING_UPLOAD_SERVER_HOSTNAME = 'localhost'
BILLING_UPLOAD_SERVER_USERNAME = 'admin'
BILLING_UPLOAD_SERVER_PASSWORD = 'test_password'
# ssh RSA key (either this or password above is necessary)
# BILLING_UPLOAD_SERVER_KEYFILE = '/home/user/.ssh/billing_upload.id_rsa'
BILLING_UPLOAD_SERVER_DIRECTORY = '/opt/billing_reports/'
