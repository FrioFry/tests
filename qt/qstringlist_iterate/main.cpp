#include <QStringList>
#include <QDebug>
#include <typeinfo>
#include <type_traits>


int main(int argc, char**argv)
{
    QStringList ss;
    ss << "1" << "2" << "3";

    const QStringList qq(ss);
    for (auto &&x: qq){
        qDebug() << typeid(x).name();
        qDebug() << std::is_const<decltype(x)>::value << " - is const" << std::is_reference<decltype(x)>::value << " -is ref";
        qDebug() << std::is_lvalue_reference<decltype(x)>::value;
        qDebug() << x;
    }


    for (const auto &x: qq) {
        qDebug() << typeid(x).name();
        qDebug() << std::is_const<decltype(x)>::value << " - is const";
        qDebug() << std::is_lvalue_reference<decltype(x)>::value;
        qDebug() << x;
    }

    qDebug() << "hi";
}
