#include <QDebug>
#include <QtConcurrent>
#include <QtWidgets>
#include "asyncfuture.h"

#include <functional>

using namespace QtConcurrent;
void myToUpper() {
  qDebug() << Q_FUNC_INFO << "ran";
    return ;
}

#include <asyncfuture/asyncfuture.h>
#include <Models/TextThread.hpp>

class TextThreadsTableImpl;

class TextThreadsTable : public QObject
{
    Q_OBJECT

public:
    explicit TextThreadsTable(gsl::not_null<TextThreadsTableImpl *> table,
                              QObject *parent = nullptr);

    AsyncFuture::Deferred<QSharedPointer<TextThread>> insert(QSharedPointer<TextThread> thread);
    AsyncFuture::Deferred<void> update(QSharedPointer<TextThread> thread);
  };

void read1(int arg) {
  qDebug() << Q_FUNC_INFO << arg;
  int x=10;
  for (int i = 0; i < 10000000; i++) {
    x += 1;
  }
}

void doRead1(int arg1) {
  auto deferredRead = AsyncFuture::deferred<void>();
  QMetaObject::invokeMethod(_table, [=]() mutable {
      if (_table->update(thread)) {
          deferredUpdate.complete();
      } else {
          deferredUpdate.cancel();
      }
});

return deferredUpdate;
}



void handle() {

}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    // Create a progress dialog.
    QProgressDialog dialog;
    dialog.setLabelText(QString("test async"));

    {
        auto defer = AsyncFuture::deferred<void>();

        auto worker = [=]() mutable {
            QFuture<void> future = QtConcurrent::run(myToUpper);
            defer.complete(future);
        };

        QtConcurrent::run(worker);
        auto future = defer.future();

        /////////////////////////
        /* End of sample code */

        //future.waitForFinished();
        qDebug() << "FINISHED";
    }

    // Display the dialog and start the event loop.
    dialog.exec();
}
