import QtQuick 2.12
import QtQuick.Window 2.12
import QtWebEngine 1.5
import QtQuick.Controls 2.5
import QtWebSockets 1.1

Window {
    width: 1024
    height: 750
    visible: true
    WebEngineView {
        id: webview
        anchors.fill: parent
        //url: "https://www.qt.io"
        url: "step-02/index.html"
        onFeaturePermissionRequested: {
            grantFeaturePermission(securityOrigin, feature, true)
        }
    }
    Rectangle  {

        color: "yellow"
        width: 300
        height: 100
        Button {
            text: "clickme"
            onClicked: {
                webview.runJavaScript("document.title", function(result) { console.log(result); });
            }
        }
    }
}
