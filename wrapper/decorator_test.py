#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
минимальный пример, помогающий разобраться как работают цепочки декораторов(функций)
в swiftcache decorators.py
"""
from functools import wraps

def post_only(f):
    @wraps(f)
    def _post_only(*args, **kwargs):
    	print "_post_only(%s)" % args[0]
    	return f(*args, **kwargs)
    return _post_only


def swift_auth(perm=None, allow_expired_password=False):
    def wrapper(f):
        p = perm or "permissions.PERM.MANAGER"
        return _swift_auth(p, allow_expired_password, f)
    return wrapper


def auto(**dkwargs):
    def _auto(f):
        @wraps(f)
        def _autocontext(*args, **kwargs):
			print "_autocontext ", dkwargs
			rv = f(*args, **kwargs)
        return _autocontext
    return _auto


def auth_context(f):
    @wraps(f)
    def _auth_context(*args, **kwargs):
    	ip = args[0] + ".user.ip"
    	user = args[0] + ".user"
    	euser = args[0] + ".euser"
    	print "_auth_context ", args, kwargs
        return auto(user=user, euser=euser, ip=ip)(f)(*args, **kwargs)
    return _auth_context


# суперпозиция swift_auth и auth_context.
# 
def swift_no_csrf_view(perm=None, allow_expired_password=False):
    def _swift_view(f):
    	print "_swift_view", perm, allow_expired_password
        return swift_auth(perm, allow_expired_password)(auth_context(f))
    return _swift_view


def swift_view(perm="one", allow_expired_password=False):
    def _swift_csrf_protect(f):
    	print "_swift_csrf_protect", perm, allow_expired_password
        return swift_no_csrf_view(perm, allow_expired_password)(f)
    return _swift_csrf_protect


def _swift_auth(perm, allow_expired_password, func):
    def __swift_auth(self, *args, **kwargs):
        @auto()
        def _auth_function(*args, **kwargs):
            print "_auth_function", args, kwargs
            return True
        _auth_function(args[0], username='credentials[0]', password='credentials[1]')
        return func(*args, **kwargs)
    return __swift_auth



@post_only
@swift_view()
def add_section(p1, p2):
    print "add_section ", p1, p2


add_section("a", "b")
