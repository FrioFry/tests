#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import (wraps, update_wrapper)
def auto(**dkwargs):
    def _auto(f):
        #@wraps(f)
        def _autocontext(*args, **kwargs):
            print "_autocontext ", dkwargs, f, args, kwargs
            rv = f(*args, **kwargs)
        return _autocontext
    return _auto

def auth_context(f):
    #s(f)
    def _auth_context(*args, **kwargs):
        ip = args[0] + ".user.ip"
        user = args[0] + ".user"
        euser = args[0] + ".euser"
        print "_auth_context ", args, kwargs, f
        return auto(user=user, euser=euser, ip=ip)(f)(*args, **kwargs)
    return _auth_context

def _swift_auth(perm, allow_expired_password, func):
    #@wraps(func)
    def __swift_auth(*args, **kwargs):
        @auto()
        def _auth_function(*args, **kwargs):
            print "_auth_function", args, kwargs, func
            return True
        print "__swift_auth ", args, kwargs, func
        _auth_function(args[0], username='credentials[0]', password='credentials[1]')
        return func(*args, **kwargs)
    return __swift_auth


def swift_auth(perm=None, allow_expired_password=False):
    def wrapper(f):
        p = perm or "permissions.PERM.MANAGER"
        print "swifth_auth.wrapper", p, f
        return _swift_auth(p, allow_expired_password, f)
    return wrapper

def swift_no_csrf_view(perm=None, allow_expired_password=False):
    def _swift_view(f):
        print "_swift_view", perm, allow_expired_password, f
        return swift_auth(perm, allow_expired_password)(auth_context(f))
    return _swift_view

def swift_view(perm="one", allow_expired_password=False):
    def _swift_csrf_protect(f):
        print "_swift_csrf_protect", perm, allow_expired_password, f
        return swift_no_csrf_view(perm, allow_expired_password)(f)
    return _swift_csrf_protect

#@swift_view() #= _swift_csrf_protect(f) = swift_no_csrf_view(f) = swift_auth()(auth_context(f) = auto(user, ip)(f) = _autocontex(f) = f(args)) = _swift_auth(args, f) = f
def add_section(p1, p2):
    print "add_section ", p1, p2

#x = swift_view()(add_section)
#x = swift_no_csrf_view("1", False)(add_section)
#x = swift_auth("1", False)(add_section)
x = _swift_auth("1", False, add_section)
#x = add_section
print x('3', '4')
#print add_section.__name__
#add_section('1', '2')

print "\n\n#################################\n"
def test2():

    def view(p=None):
        def _view(f):
            print "view", p
            return f    
        return _view

    @view()
    def view_f(p1, p2):
        print "view_f", p1, p2

    view_f(1,2)

    def dec1(f):
        #@wraps(f)
        def _dec1(*args, **kwargs):
            print "1", args, kwargs
            f(*args, **kwargs)
        return _dec1

    def dec2(f):
        #@wraps(f)
        def _dec2(*args, **kwargs):
            print "2", args, kwargs
            f(*args, **kwargs)
        return _dec2


    print "first without dec"
    def f(p1, p2):
        print "f, ", p1, p2
    x = dec2(dec1(f))
    x(1, 2)
    print x.__name__

    print "second with dec"
    @dec2
    @dec1
    def f2(p1, p2):
        print "f2, ", p1, p2

    f2(1, 2)
    print f2.__name__