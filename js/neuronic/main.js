(function() {
    var sock = null;
    var heartbeatTimeoutFuture;
    var textNode;
    var messagesNode;
    var sendButtonNode;
    var nextButtonNode;
    var writingNode;
    var scrollFuture;
    var readyToSend = false;
    var voted = false;
    var connecting = false;
    var writing = false;

    /**
     * On document ready
      */
    $(function() {
        textNode = $("#text");
        sendButtonNode = $("#sendButton");
        nextButtonNode = $("#nextButton");
        yesBotButton = $("#yesBotButton");
        notBotButton = $("#notBotButton");
        messagesNode = $("#messages");
        writingNode = $("#writing");
        $("#form").submit(function(e) {
            e.preventDefault();
            sendMsg();
        });
        textNode.keyup(function() {
            if (!readyToSend) {
                writing = false;
                return;
            }
            if (textNode.val().length == 0) {
                if (writing) {
                    //send not writing
                    writing = false;
                    console.log("tyuping");
                    sock.send("9");
                }
            } else {
                if (!writing) {
                    //send writing
                    writing = true;
                    console.log("not tyuping");
                    sock.send("8");
                }
            }
        });
        yesBotButton.click(function() {
            if (!readyToSend) return;
            cleanChat();
            addMsg("Собеседник робот", "sys");
            addMsg("Сессия завершена", "sys");
            console.log("clicked yes");
            sock.send("a,1");
            setReadyToSend(false);
            sock.send("1");
        });
        notBotButton.click(function() {
            if (!readyToSend) return;
            cleanChat();
            addMsg("Собеседник не робот", "sys");
            addMsg("Сессия завершена", "sys");
            console.log("clicked no");
            sock.send("a,2");
            setReadyToSend(false);
            sock.send("1");
        });
        reconnect();
    });

    /**
     * Common functions
     */

    function reconnect() {
        if (!connecting) {
            addMsg("Подключение к серверу...", "con");
            connecting = true;
        }
        sock = new SockJS('http://127.0.0.1:3000/echo', null, {});
        sock.onopen = function () {
            connecting = false;
            addMsg("Ожидание нового собеседника...", "sys");
            resetHeartbeatTimeout();
            setReadyToSend(false);
            console.log("OPen sock");
            sock.send("1");
        };

        sock.onclose = function () {
            cancelHeartbeatTimeout();
            sock = null;
            setReadyToSend(false);
            console.log("Close sock");
            sock.send("1");
            setTimeout(reconnect, 3000);
        };

        sock.onmessage = function (e) {
            resetHeartbeatTimeout();
            var data = e.data;
            var cmd = data.charAt(0);
            switch (cmd) {
                case '2':
                    // friend connected
                    setReadyToSend(true);
                    addMsg("Собеседник подключен, напишите что-нибудь", "sys");
                    break;
                case '4':
                    // 4 и 5 обрабатываются одинаково ниже (в 5)
                case '5':
                    //msg
                    var items = split(data, ',', 2);
                    var id = items[1];
                    var text = items[2];
                    var me = cmd == '4';
                    if (!me) writingNode.hide();
                    addMsg((me?"Я: ":"Чатбот: ")+ text, me ? "me" : "anon");
                    //ack
                    console.log("Message got");
                    sock.send("6,"+id);
                    break;
                case '7':
                    //anon disconnected
                    addMsg("Собеседник отключился", "sys");
                    addMsg("Считаете ли вы что собеседник был роботом?", "sys");
                    console.log("disconnect search new");
                    break;
                case '8':
                    //writing
                    writingNode.show();
                    scrollBottom();
                    break;
                case '9':
                    //not writing
                    writingNode.hide();
                    break;
                case 'o':
                    nextButtonNode.text("Следующий из " + data.substring(2) + " чел.");
                    break;
            }
        };

        sock.onheartbeat = resetHeartbeatTimeout;
    };

    function resetHeartbeatTimeout() {
        if (heartbeatTimeoutFuture) {
            clearTimeout(heartbeatTimeoutFuture);
        }
        heartbeatTimeoutFuture = setTimeout(heartbeatTimeout, 60000);
    };

    function cancelHeartbeatTimeout() {
        if (heartbeatTimeoutFuture) {
            clearTimeout(heartbeatTimeoutFuture);
            heartbeatTimeoutFuture = null;
        }
    };

    function heartbeatTimeout() {
        addMsg("Потеряна связь с сервером.", "con");
        sock.close();
        heartbeatTimeoutFuture = null;
    };

    // очистить чат
    function cleanMessages() {
        console.log("cleaning");
        console.log($("#messages :not(#writing)"));
        $("#messages :not(#writing,.con,.sys)").remove();
    }

    function cleanChat() {
        //$("#messages :not(#writing,.con,.sys)").remove();
        $("#messages :not(#writing)").remove();
    }
    // value==true включить чат и кнопки голосвания
    // value==false отключить чат, показат ьсколько людей в чате
    function setReadyToSend(value) {
        readyToSend = value;
        console.log(readyToSend);

        textNode.prop("disabled", !value);
        sendButtonNode.prop("disabled", !value);

        nextButtonNode.toggle(!value);
        nextButtonNode.prop("disabled", !value);

        yesBotButton.toggle(value);
        yesBotButton.prop("disabled", !value);
        notBotButton.toggle(value);
        notBotButton.prop("disabled", !value);

        if (value) {
            // очистить чат
            textNode.focus();
        } else {
            //cleanMessages();
            writingNode.hide();
        }
    };

    function sendMsg() {
        if (!readyToSend) {
            textNode.focus();
            return;
        }
        var text = textNode.val();
        if (text.length == 0) {
            textNode.focus();
            return;
        }
        console.log("send message");
        sock.send("3," + text);
        textNode.val("");
        writing = false;
        textNode.focus();
    };

    function addMsg(text, className) {
        var msg = $("<div></div>");
        if (className) msg.addClass(className);
        msg.text(text);
        msg.insertBefore(writingNode);
        scrollBottom();
    };

    function scrollBottom() {
        if (!scrollFuture) {
            scrollFuture = setTimeout(function() {
                scrollFuture = null;
                messagesNode.scrollTop(messagesNode.prop("scrollHeight"));
            }, 0);
        }
    };

    function split(str, separator, limit) {
        str = str.split(separator);

        if(str.length > limit) {
            var ret = str.splice(0, limit);
            ret.push(str.join(separator));

            return ret;
        }

        return str;
    };

})();
