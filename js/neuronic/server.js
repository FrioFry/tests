var http = require('http');
//var port = process.env.PORT || 3000;
var sockjs = require('sockjs');
var fs = require('fs');

var echo = sockjs.createServer();

var wait_queue = [];                // Те кто не в комнате еще
var dialogs = [];                   // все активные диалоги
var dialog_by_conn = {};            // Диалог по соединению

dialog_counter = 0;                 // Идентификатор для нового диалога
user_counter = 0;                   // количество пользователей активных

 // Отправить сообщение (3)
function send_message(dialog, conn, text) {
    // Сохранить сообщение, чтобы потом сохранить в файл
    id = 1;
    if (conn == dialog.conn2) {
        id = 2;
    }
    dialog.messages.push([id, text]);
    msg_id = dialog.messages.length;
    // Отправить сообщения обоим участникам
    if (conn == dialog.conn1) {
        dialog.conn1.write("4," + msg_id + "," + text);
        dialog.conn2.write("5," + msg_id + "," + text);
    } else {
        dialog.conn1.write("5," + msg_id + "," + text);
        dialog.conn2.write("4," + msg_id + "," + text);
    }
}

// Создать диалог (комнату) (1)
function new_dialog(conn1, conn2) {
    // Пустой диалог (соединение1, соединение2, счетчик сообщений, сами сообщения, номер диалога)
    dialog = {conn1: conn1, conn2: conn2, msg_id: 0, messages: [], dialog_id: dialog_counter};
    dialog_counter++;
    dialogs.push(dialog); // В список активных диалогов
    dialog_by_conn[conn1.id] = dialog;
    dialog_by_conn[conn2.id] = dialog;

    // отправим обоим участникам что они в диалоге
    conn1.write("2");
    conn2.write("2");
}

// Вернуть дату в строке
function current_date_str() {
    var v = new Date();
    return v.getFullYear() + "_" +
        v.getMonth() + "_" +
        v.getDay()+ "-" +
        v.getHours() + "_" +
        v.getMinutes() + "_" +
        v.getSeconds() + "_" +
        v.getMilliseconds();
}

// Сохранить переписку в файл (его имя <дата> +  <id_шник диалогоа>.log)
function save_dialog_to_file(dialog) {
    if (dialog.messages.length) { // пустые диалоги не сохраням
        var filename = current_date_str() + "-" + dialog.dialog_id + ".log";
        var text = JSON.stringify(dialog.messages);
        fs.writeFile(filename, text, function(err) {
            if(err) {
                return console.log(err);
            }
            //console.log("dialog " + dialog.dialog_id + " was saved to " + filename);
        });
    }
}

// Закончить диалог (7)
function end_dialog(dialog, conn) {
    //console.log("ending dialog by " + conn.id);
    if (conn != dialog.conn1) {
        dialog.conn1.write("7");
    }
    if (conn != dialog.conn2) {
        dialog.conn2.write("7");
    }
    delete dialog_by_conn[dialog.conn1.id];
    delete dialog_by_conn[dialog.conn2.id];
    dialog_id = dialog.dialog_id;
    // Сохранить сообщения
    save_dialog_to_file(dialog);
    delete dialogs[dialog_id];
}

// Найти свой диалог
function find_dialog(conn) {
    if (conn.id in dialog_by_conn) {
        return dialog_by_conn[conn.id];
    }
    // не нашли
    return undefined;
}

// Получить подключение собеседника
function get_friend_conn(dialog, conn) {
    friend = dialog.conn1;
    if (dialog.conn1 == conn) {
        friend = dialog.conn2;
    }
    return friend;
}

// Статус пишу (8)
function on_typing(dialog, conn) {
    conn = get_friend_conn(dialog, conn);
    conn.write("8");
}

// Статус не пишу (9)
function on_stop_typing(dialog, conn) {
    conn = get_friend_conn(dialog, conn);
    conn.write("9");
}

echo.on('connection', function (conn) {
    setInterval(function () {
        conn.write('');
    }, 55000);

    user_counter++;                     // увеличим кол-во пользователей

    // Диалога пока нет
    var dialog = undefined;

    conn.on('data', function (message) {
        var cmd = message.charAt(0);
        //console.log("New command: " + cmd + ", from " + conn.id);

        // Попробуем найти диалог, если кто-то уже к нам подцепился
        if (!dialog) {
            dialog = find_dialog(conn);
        }

        switch (cmd) {
            case '1': // Ищет собеседника
                // Для начала отправим ему количество пользователей на сервере
                conn.write("o," + user_counter);

                // Если мы уже в комнате _> удалимся и скажем собеседнику "пока"
                if (dialog) {
                    end_dialog(dialog, conn);
                    dialog = undefined;
                }

                // Если в очереди ожидающих собеседника кто-то есть, соеденимся с ним
                if (wait_queue.length > 0) {
                    var random_id = Math.floor(Math.random()*wait_queue.length);
                    friend_conn = wait_queue[random_id];
                    wait_queue.splice(random_id, 1); // удалить из очереди ожиждающих
                    if (conn != friend_conn) {
                        new_dialog(conn, friend_conn);
                    } else {
                        wait_queue.push(conn);
                    }
                }
                else {
                    wait_queue.push(conn); // добавить в очередь ожидающих
                }
                break;
            case '3': // Отправил сообщение
                if (dialog) {
                    var text = message.substr(2);
                    send_message(dialog, conn, text);
                }
                break;
            case '6': //подтверждение получения сообщения, приходит вместе с id сообщения
                break;
            case '8': //печатает (переслать собеседнику)
                if (dialog) {
                    on_typing(dialog, conn);
                }
                break;
            case '9'://не печатает (переслать собеседнику)
                if (dialog) {
                    on_stop_typing(dialog, conn);
                }
                break;
            case 'a': // вердикт о собеседнике
                break;
        }
    });

    conn.on('close', function () {
        user_counter--;
        if (!dialog) {
            dialog = find_dialog(conn);
        }
        //console.log("closing " + conn.id + " " + dialog);
        if (dialog) {
            end_dialog(dialog, conn);
            dialog = undefined;
        }
    });
});

var server = http.createServer();
echo.installHandlers(server, {prefix: '/echo'});
server.listen(3000, '0.0.0.0');
//server.listen(port);
